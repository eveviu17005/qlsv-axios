// pending, resolve, reject
// axios ~ bất đồng bộ
const BASE_URL = "https://633ec06583f50e9ba3b762a2.mockapi.io";

var batLoading = function () {
  document.getElementById("loading").style.display = "flex";
};
var tatLoading = function () {
  document.getElementById("loading").style.display = "none";
};

// lay danh sach sinh vien service

var fetchDssvService = function () {
  batLoading();
  axios({
    url: `${BASE_URL}/SV`,
    method: "GET",
  })
    .then(function (response) {
      renderDanhSachSinhVien(response.data);
      tatLoading();
    })
    .catch(function () {
      tatLoading();
      Swal.fire("Lấy dữ liệu thất bại");
    });
};
// chạy lần đầu khi load trang
fetchDssvService();

// render danh sách sinh viên

var renderDanhSachSinhVien = function (listSv) {
  var contentHTML = "";
  listSv.forEach(function (sv) {
    contentHTML += `<tr>
                <td>${sv.ma}</td>
                <td>${sv.ten}</td>
                <td>${sv.email}</td>
                <td>${((sv.toan*1 + sv.ly*1 + sv.hoa*1) / 3).toFixed(2)}</td>
                <td>
                <button onclick="layThongTinChiTietSv('${
                  sv.ma
                }')" class="btn btn-primary">Sửa</button>
                <button onclick="xoaSv(${
                  sv.ma
                })" class="btn btn-danger">Xoá</button>
                </td>
              </tr>`;
             
              
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

// xoa sv

var xoaSv = function (idSv) {
  batLoading();
  axios({
    url: `${BASE_URL}/SV/${idSv}`,
    method: "DELETE",
  })
    .then(function () {
      tatLoading();
      fetchDssvService();
      Swal.fire("Xoá thành công");
    })
    .catch(function () {
      tatLoading();
      Swal.fire("Xoá thành công");
    });
};

// // them sinh vien

var themSv = function () {
  var sv = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/SV`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      tatLoading();
      Swal.fire("Thêm thành công");
      fetchDssvService();
    })
    .catch(function (err) {
      tatLoading();
      Swal.fire("Thêm thất bại");
    });
};

var layThongTinChiTietSv = function (idSv) {
  batLoading();
  axios
    .get(`${BASE_URL}/SV/${idSv}`)
    .then(function (res) {
      tatLoading();

      showThongTinLenForm(res.data);
      Swal.fire("Lấy thông tin thành công");
    })
    .catch(function () {
      tatLoading();
      Swal.fire("Lấy thông tin thất bại");
    });
};

var capNhatSv = function () {
  var sv = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/SV/${sv.ma}`,
    method: "PUT",
    data: sv,
  })
    .then(function () {
      tatLoading();
      fetchDssvService();
      Swal.fire("Cập nhật thành công");
    })
    .catch(function () {
      tatLoading();
      Swal.fire("Cập nhật thất bại");
    });
};
